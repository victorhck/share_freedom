## Share Freedom

![](https://victorhckinthefreeworld.files.wordpress.com/2018/04/share_freedom.png)

Este es un código para compartir contenidos de webs o blogs en redes sociales libres y federadas como Mastodon, diaspora, GNUsocial o Hubzilla.
El código está creado por __Kim__ que ha publicado bajo licencia GNU GPL v3.

Este _fork_ crea a partir del código de Kim un servicio on-line utilizando GitLab Pages por medio del cual compartir en esas redes sociales. La idea es añadir un botón en nuestro blog Wordpress y en él enlazar al servicio de [GitLab Pages](https://victorhckinthefreeworld.com/2016/08/31/alojar-una-pagina-html-utilizando-gitlab-pages/) pasándole como parámetros el título del artículo y la dirección url del mismo.

En Wordpress deberemos ir al menú __Ajustes → Compartir__ y en el siguiente menú añadir lo necesario:

![](https://victorhckinthefreeworld.files.wordpress.com/2014/01/diaspora2.png)

* En Nombre del servicio: ponemos el texto que queremos que aparezca en nuestro botón
* En Compartiendo URL: pondremos lo siguiente: _https://victorhck.gitlab.io/share_freedom/?text=%post_title%&url=%post_url%_
* En URL del icono: la dirección de la imagen que quieres que se muestre como icono al lado del botón

Nos creará el botón y deberemos activarlo y guardar los cambios. Ya estará disponible en el blog para que quien lo visite pueda compartirlo en redes sociales libres. 

Al pinchar en el botón, esto nos abrirá el servicio online con el texto y url y ya podremos compartir el contenido en las mencionadas redes sociales libres.

También puedes utilizar el [código de Kim](https://gitlab.com/d3fc0n4/mastodon_share) para añadir el botón en tu propia web o wordpress si lo tienes instalado en tu propio servidor.

Tienes más información, así como un video tutorial mostrando el proceso en mi blog:
* https://victorhckinthefreeworld.com/2018/04/06/como-anadir-un-boton-en-tu-wordpress-para-compartir-contenido-en-mastodon-diaspora-gnusocial-o-hubzilla/

__¡A compartir libremente!__
